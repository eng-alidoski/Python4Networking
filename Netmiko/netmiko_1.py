"""
Netmiko Single Switch Script

This script connects to a single Cisco switch using Netmiko library and performs various operations such as retrieving VLAN information and creating VLANs.

Author: AliDoski
Date: 27th-6-2023

Usage:
- Update the device details (IP, username, password) in the SW1 dictionary.
- Run the script to execute the operations.

Note: Make sure to have Netmiko library installed before running this script.
"""

from netmiko import ConnectHandler

SW1 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.100.123',
    'username': 'admin',
    'password': 'admin'
}

net_connect = ConnectHandler(**SW1)
output = net_connect.send_command('show vlan')
print (output)

config_commands = ['int loop 0', 'ip address 1.1.1.1 255.255.255.0']
output = net_connect.send_config_set(config_commands)
print (output)

for n in range (2,10):
    print ("Creating VLAN " + str(n))
    config_commands = ['vlan ' + str(n), 'name Python_VLAN ' + str(n)]
    output = net_connect.send_config_set(config_commands)
    print (output)
