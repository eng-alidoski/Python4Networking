"""
Netmiko Single Switch Script

This script connects to multiple Cisco switches using Netmiko library and performs various operations such as retrieving VLAN information and creating VLANs.

Author: AliDoski
Date: 27th-6-2023

Usage:
- Update the device details (IP, username, password) in the switches file.
- Update the commands in the SW-Commands-File.
- Run the script to execute the operations.

Required Libraries:
- netmiko: A multi-vendor library to simplify SSH connections to network devices.
- getpass: A library for secure password input from the user.

Note: Make sure to have the required libraries installed before running this script.
"""

from netmiko import ConnectHandler
import getpass


# Asking for credentials before running the script on devices to make any changes
user = input("Enter your remote account: ")
password = getpass.getpass()

# Open the switches file and read the IP addresses
with open('switches') as f:
    sw_list = f.read().splitlines()

# For switch configuration
with open('SW-Commands-File') as f:
    switch_commands = f.read().splitlines()


def configure_device(device, commands):
    """
    Connects to the device using Netmiko and sends the configuration commands.

    Args:
        device (dict): Device details containing device_type, IP, username, and password.
        commands (list): List of configuration commands.

    Returns:
        None
    """
    device['username'] = user
    device['password'] = password

    net_connect = ConnectHandler(**device)
    output = net_connect.send_config_set(commands)
    print(output)


# Iterate over the switch IP addresses
for switch in sw_list:
    print('Connecting to device: ' + switch)
    ip_address_of_device = switch
    ios_device = {
        'device_type': 'cisco_ios',
        'ip': ip_address_of_device,
    }
    configure_device(ios_device, switch_commands)
