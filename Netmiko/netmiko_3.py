"""
Cisco Device Configuration Script

This script automates the configuration process for Cisco devices by applying standard IP Services configurations such as NTP, SNMP, ACL, and interface management descriptions. It leverages the Netmiko library and ThreadPoolExecutor to configure multiple devices in parallel.

Author: AliDoski
Date: 28.6.2023

Usage:

    Ensure that the Netmiko library is installed.
    Prepare the configuration files:
        Create two separate command files:
            'SW-Commands-File': Contains the desired commands for switch configurations.
            'RT-Commands-File': Contains the desired commands for router configurations.
    Update the device details (IP addresses) in the SW1, SW2, R1, and R2 dictionaries.
    Run the script and provide the requested credentials (username and password).
    The script will connect to each device concurrently and apply the specified configurations.

Note: This script assumes that you have prior knowledge of the desired configuration commands for each device.

"""

from netmiko import ConnectHandler
import getpass
from concurrent.futures import ThreadPoolExecutor

SW1 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.100.123',
}

SW2 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.100.124',
}

R1 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.100.153',
}

R2 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.100.154',
}

# Asking for credentials before running the script on devices to make any changes
user = input("Enter your remote account: ")
password = getpass.getpass()

# For Switch configuration
with open('SW-Commands-File') as f:
    switch_commands = f.read().splitlines()

all_switches = [SW1, SW2]

# For Router configuration
with open('RT-Commands-File') as f:
    router_commands = f.read().splitlines()

all_routers = [R1, R2]

def configure_device(device, commands):
    device['username'] = user
    device['password'] = password

    net_connect = ConnectHandler(**device)
    output = net_connect.send_config_set(commands)
    print(output)

# Create a ThreadPoolExecutor with a maximum of 5 concurrent threads
executor = ThreadPoolExecutor(max_workers=5)

# Submit the configuration tasks for switches
switch_futures = [executor.submit(configure_device, switch, switch_commands) for switch in all_switches]

# Submit the configuration tasks for routers
router_futures = [executor.submit(configure_device, router, router_commands) for router in all_routers]

# Wait for all switch tasks to complete
for future in switch_futures:
    future.result()

# Wait for all router tasks to complete
for future in router_futures:
    future.result()
