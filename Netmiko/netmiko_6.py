"""
Netmiko Multiple Devices Script

This script connects to multiple Cisco switches and routers using Netmiko library and performs various operations such as retrieving VLAN information and creating VLANs.

Author: AliDoski
Date: 27th-6-2023

Usage:
- Update the device details (IP, username, password) in the switches and routers files.
- Update the commands in the SW-Commands-File and RT-Commands-File.
- Run the script to execute the operations.

Required Libraries:
- netmiko: A multi-vendor library to simplify SSH connections to network devices.
- getpass: A library for secure password input from the user.
- concurrent.futures: A library for executing tasks concurrently using ThreadPoolExecutor.

Error Handling:
- AuthenticationException: Raised when there is an authentication failure for a device.
- NetMikoTimeoutException: Raised when a timeout occurs while attempting to connect to a device.
- EOFError: Raised when there is an end-of-file error while attempting to connect to a device.
- SSHException: Raised when there is an issue with SSH connectivity to a device.
- Other Exceptions: Any other unknown errors encountered during device configuration.

Note: Make sure to have the required libraries installed before running this script.
"""

import getpass
from concurrent.futures import ThreadPoolExecutor
from netmiko import ConnectHandler
from netmiko.exceptions import NetMikoTimeoutException, AuthenticationException, SSHException


# Asking for credentials before running the script on devices to make any changes
user = input("Enter your remote account: ")
password = getpass.getpass()

# Open the switches file and read the IP addresses
with open('switches', encoding='utf-8') as f:
    sw_list = f.read().splitlines()

# Open the routers file and read the IP addresses
with open('routers', encoding='utf-8') as f:
    rt_list = f.read().splitlines()

# For switch configuration
with open('SW-Commands-File', encoding='utf-8') as f:
    switch_commands = f.read().splitlines()

# For router configuration
with open('RT-Commands-File', encoding='utf-8') as f:
    router_commands = f.read().splitlines()


def configure_device(device, commands):
    """
    Connects to the device using Netmiko and sends the configuration commands.

    Args:
        device (dict): Device details containing device_type, IP, username, and password.
        commands (list): List of configuration commands.

    Returns:
        None
    """
    device['username'] = user
    device['password'] = password

    try:
        net_connect = ConnectHandler(**device)
    except AuthenticationException:
        print('Authentication failure for device: ' + device['ip'])
        return
    except NetMikoTimeoutException:
        print('Timeout to device: ' + device['ip'])
        return
    except EOFError:
        print('End of file while attempting device: ' + device['ip'])
        return
    except SSHException:
        print('SSH Issue. Are you sure SSH is enabled for device: ' + device['ip'])
        return
    except Exception as unknown_error:
        print('Some other error occurred for device: ' + device['ip'] + ' - ' + str(unknown_error))
        return

    output = net_connect.send_config_set(commands)
    print(output)

    # Disconnect from the device
    net_connect.disconnect()


# Create a ThreadPoolExecutor with a maximum of 5 concurrent threads
executor = ThreadPoolExecutor(max_workers=5)

# Iterate over the switch IP addresses
for switch in sw_list:
    print('Connecting to switch: ' + switch)
    switch_device = {
        'device_type': 'cisco_ios',
        'ip': switch,
    }
    executor.submit(configure_device, switch_device, switch_commands)

# Iterate over the router IP addresses
for router in rt_list:
    print('Connecting to router: ' + router)
    router_device = {
        'device_type': 'cisco_ios',
        'ip': router,
    }
    executor.submit(configure_device, router_device, router_commands)

# Shutdown the ThreadPoolExecutor
executor.shutdown()
