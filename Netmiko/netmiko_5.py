"""
Netmiko Configuration Script

This script connects to multiple Cisco switches and routers using Netmiko library and performs various configuration operations concurrently. It supports parallel execution of tasks using ThreadPoolExecutor.

Author: AliDoski
Date: 27th-6-2023

Usage:
1. Update the device IP addresses in the 'switches' and 'routers' files.
2. Update the configuration commands in the 'SW-Commands-File' and 'RT-Commands-File'.
3. Run the script to execute the configuration operations on the devices.

Required Libraries:
- netmiko: A multi-vendor library for SSH connections to network devices.
- getpass: A library for secure password input from the user.
- concurrent.futures: A library for executing tasks concurrently using ThreadPoolExecutor.

--- IMPORTANT ---
Make sure to have the required libraries installed and the device details and commands properly configured before running this script.

--- File Descriptions ---
1. 'switches': A text file containing the IP addresses of the Cisco switches.
2. 'routers': A text file containing the IP addresses of the Cisco routers.
3. 'SW-Commands-File': A text file containing the configuration commands for switches.
4. 'RT-Commands-File': A text file containing the configuration commands for routers.

"""

# Import required libraries
from netmiko import ConnectHandler
import getpass
from concurrent.futures import ThreadPoolExecutor


# Rest of the script...

"""
Please refer to the script usage and file descriptions above to understand how to use the script and configure the required files.

The script connects to the devices using the provided IP addresses, retrieves the configuration commands from the respective files, and performs the configuration operations concurrently using ThreadPoolExecutor. The execution is parallelized to speed up the configuration process.

Make sure to replace the file contents and update the device details and commands according to your network configuration.

Remember to install the necessary libraries and verify the correctness of the files before running the script.
"""


# Asking for credentials before running the script on devices to make any changes
user = input("Enter your remote account: ")
password = getpass.getpass()

# Open the switches file and read the IP addresses
with open('switches') as f:
    sw_list = f.read().splitlines()

# Open the routers file and read the IP addresses
with open('routers') as f:
    rt_list = f.read().splitlines()

# For switch configuration
with open('SW-Commands-File') as f:
    switch_commands = f.read().splitlines()

# For router configuration
with open('RT-Commands-File') as f:
    router_commands = f.read().splitlines()


def configure_device(device, commands):
    """
    Connects to the device using Netmiko and sends the configuration commands.

    Args:
        device (dict): Device details containing device_type, IP, username, and password.
        commands (list): List of configuration commands.

    Returns:
        None
    """
    device['username'] = user
    device['password'] = password

    net_connect = ConnectHandler(**device)
    output = net_connect.send_config_set(commands)
    print(output)


# Create a ThreadPoolExecutor with a maximum of 5 concurrent threads
executor = ThreadPoolExecutor(max_workers=5)

# Iterate over the switch IP addresses
for switch in sw_list:
    print('Connecting to switch: ' + switch)
    switch_device = {
        'device_type': 'cisco_ios',
        'ip': switch,
    }
    executor.submit(configure_device, switch_device, switch_commands)

# Iterate over the router IP addresses
for router in rt_list:
    print('Connecting to router: ' + router)
    router_device = {
        'device_type': 'cisco_ios',
        'ip': router,
    }
    executor.submit(configure_device, router_device, router_commands)

# Shutdown the ThreadPoolExecutor
executor.shutdown()
