"""
Cisco Device Configuration Script

This script connects to multiple Cisco devices and configures loopback interfaces with descriptions. The loopback interfaces are created with numbers ranging from 2 to 9.

Author: AliDoski
Date: 28th-6-2023

Usage:

    Ensure that the Netmiko library is installed.
    Update the device details (IP addresses, usernames, and passwords) in the SW1 and SW2 dictionaries.
    Run the script.
    The script will connect to each device and configure loopback interfaces with descriptions.

"""

from netmiko import ConnectHandler

SW1 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.100.123',
    'username': 'admin',
    'password': 'admin'
}

SW2 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.100.124',
    'username': 'admin',
    'password': 'admin'
}


all_devices = [SW1, SW2]

for devices in all_devices:
    net_connect = ConnectHandler(**devices)
    for n in range (2,10):
       print ("loopback interfaces " + str(n))
       config_commands = ['int lo ' + str(n), 'Description Loopback ' + str(n)]
       output = net_connect.send_config_set(config_commands)
       print (output)
