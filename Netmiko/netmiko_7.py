"""
Netmiko Multiple Devices Script

This script connects to multiple Cisco switches and routers using Netmiko and performs various operations.

Author: AliDoski
Date: 27th-6-2023

Usage:
1. Update device details (IP, username, password) in 'switches' and 'routers' files.
2. Update commands in 'SW-Commands-File' and 'RT-Commands-File'.
3. Run the script to execute the operations.

Required Libraries:
- netmiko: Simplifies SSH connections to network devices.
- getpass: Securely inputs password from the user.
- concurrent.futures: Executes tasks concurrently using ThreadPoolExecutor.

Error Handling:
- AuthenticationException: Failed device authentication.
- NetMikoTimeoutException: Connection timeout.
- EOFError: End-of-file error during connection.
- SSHException: SSH connectivity issue.
- Other Exceptions: Unknown errors during device configuration.

Steps:
1. Prompt user for remote account credentials.
2. Read switch and router IP addresses.
3. Read switch and router commands.
4. Define 'configure_device' function to connect and send configuration commands.
5. Connect to device, handle exceptions, and send commands.
6. Create ThreadPoolExecutor with 5 concurrent threads.
7. Submit configuration tasks for switches and routers.
8. Shutdown ThreadPoolExecutor.
9. Check device software versions.
10. Execute commands based on software version.
11. Print command output.

Note: Install required libraries before running.
"""


import getpass
from concurrent.futures import ThreadPoolExecutor
from netmiko import ConnectHandler
from netmiko.exceptions import NetMikoTimeoutException, AuthenticationException, SSHException


# Asking for credentials before running the script on devices to make any changes
user = input("Enter your remote account: ")
password = getpass.getpass()

# Open the switches file and read the IP addresses
with open('switches', encoding='utf-8') as f:
    sw_list = f.read().splitlines()

# Open the routers file and read the IP addresses
with open('routers', encoding='utf-8') as f:
    rt_list = f.read().splitlines()

# For switch configuration
with open('SW-Commands-File', encoding='utf-8') as f:
    switch_commands = f.read().splitlines()

# For router configuration
with open('RT-Commands-File', encoding='utf-8') as f:
    router_commands = f.read().splitlines()


def configure_device(device, commands):
    """
    Connects to the device using Netmiko and sends the configuration commands.

    Args:
        device (dict): Device details containing device_type, IP, username, and password.
        commands (list): List of configuration commands.

    Returns:
        None
    """
    device['username'] = user
    device['password'] = password

    try:
        net_connect = ConnectHandler(**device)
    except AuthenticationException:
        print('Authentication failure for device: ' + device['ip'])
        return
    except NetMikoTimeoutException:
        print('Timeout to device: ' + device['ip'])
        return
    except EOFError:
        print('End of file while attempting device: ' + device['ip'])
        return
    except SSHException:
        print('SSH Issue. Are you sure SSH is enabled for device: ' + device['ip'])
        return
    except Exception as unknown_error:
        print('Some other error occurred for device: ' + device['ip'] + ' - ' + str(unknown_error))
        return

    output = net_connect.send_config_set(commands)
    print(output)

    # Disconnect from the device
    net_connect.disconnect()

    # Types of devices
    list_versions = ['vios_l2-ADVENTERPRISEK9-M', 
                     'VIOS-ADVENTERPRISEK9-M',
                     'C1900-UNIVERSALK9-M',
                     'C3750-ADVIPSERVICESK9-M'
                     ]

    # Check software versions
    for software_ver in list_versions:
        print ('Checking for ' + software_ver)
        output_version = net_connect.send_command('show version')
        int_version = 0 # Reset integer value
        int_version = output_version.find(software_ver) # Check software version
        if int_version > 0:
            print ('Software version found: ' + software_ver)
            break
        else:
            print ('Did not find ' + software_ver)

    if software_ver == 'vios_l2-ADVENTERPRISEK9-M':
        print ('Running ' + software_ver + ' commands')
        output = net_connect.send_config_set('SW-Commands-File')
    elif software_ver == 'VIOS-ADVENTERPRISEK9-M':
        print ('Running ' + software_ver + ' commands')
        output = net_connect.send_config_set('RT-Commands-File')
    elif software_ver == 'C1900-UNIVERSALK9-M':
        print ('Running ' + software_ver + ' commands')
        output = net_connect.send_config_set('RT-Commands-File')
    elif software_ver == 'C3750-ADVIPSERVICESK9-M':
        print ('Running ' + software_ver + ' commands')
        output = net_connect.send_config_set('SW-Commands-File')    
    print (output) 

# Create a ThreadPoolExecutor with a maximum of 5 concurrent threads
executor = ThreadPoolExecutor(max_workers=5)

# Iterate over the switch IP addresses
for switch in sw_list:
    print('Connecting to switch: ' + switch)
    switch_device = {
        'device_type': 'cisco_ios',
        'ip': switch,
    }
    executor.submit(configure_device, switch_device, switch_commands)

# Iterate over the router IP addresses
for router in rt_list:
    print('Connecting to router: ' + router)
    router_device = {
        'device_type': 'cisco_ios',
        'ip': router,
    }
    executor.submit(configure_device, router_device, router_commands)

# Shutdown the ThreadPoolExecutor
executor.shutdown()
