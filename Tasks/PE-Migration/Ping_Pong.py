import subprocess
import ipaddress
import openpyxl
from ftplib import FTP

# Create a new Excel workbook
workbook = openpyxl.Workbook()
sheet = workbook.active

# Define the IP ranges
ip_ranges = [
    ' IP RANGE 1',
    ' IP RANGE 2',
    ' IP RANGE 3',
    ' IP RANGE 4',
    ' IP RANGE 5',
    ' IP RANGE 6',
]

# Loop through the IP ranges
for ip_range in ip_ranges:
    network = ipaddress.ip_network(ip_range)

    # Loop through the IP addresses in the range
    for ip_address in network.hosts():
        ip_address_str = str(ip_address)

        # Ping the IP address
        result = subprocess.run(['ping', '-c', '1', ip_address_str], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        # Check the return code
        if result.returncode == 0:
            print(f'{ip_address_str} is reachable')
            sheet.append([ip_address_str, 'Reachable'])

# Save the workbook to an Excel file
workbook.save('reachable_ips.xlsx')

# FTP server details
ftp_host = 'YOUR FTP SERVER'
ftp_user = 'FTP USER'
ftp_password = 'FTP USER PASSWORD'

# Upload the file to FTP server
ftp = FTP(ftp_host)
ftp.login(user=ftp_user, passwd=ftp_password)
ftp.cwd('Doski')  # Change to the desired folder on the FTP server
with open('reachable_ips.xlsx', 'rb') as file:
    ftp.storbinary('STOR reachable_ips.xlsx', file)
ftp.quit()

