# Pre and Post Migration Script for Network Engineer

This script is designed to automate network tasks before and after a migration. It helps network engineers ensure a smooth transition and validate the functionality of the network.

## Prerequisites

To run this script, you need the following dependencies:

- `subprocess`: A module for running external commands and processes.
- `ipaddress`: A module for working with IP addresses and networks.
- `openpyxl`: A module for creating and modifying Excel files.
- `ftplib`: A module for interacting with FTP servers.

## Script Overview

The script performs the following tasks:

1. **Checking IP Reachability:**

   - Defines a list of IP ranges to check.
   - Loops through each IP range.
   - Loops through each IP address within the range.
   - Pings each IP address to check its reachability.
   - Records the reachable IP addresses in an Excel file.

2. **Saving Reachable IP Addresses:**

   - Creates a new Excel workbook.
   - Writes the reachable IP addresses to the workbook.
   - Saves the workbook as "reachable_ips.xlsx".

3. **Uploading to FTP Server:**

   - Specifies the FTP server details.
   - Connects to the FTP server.
   - Changes to the desired folder on the server.
   - Uploads the "reachable_ips.xlsx" file to the server.

## Usage

1. Set up the necessary dependencies mentioned above.

2. Update the script with the desired IP ranges and FTP server details.

3. Run the script to perform the following actions:
   - Check the reachability of IP addresses within the specified ranges.
   - Save the reachable IP addresses to an Excel file.
   - Upload the Excel file to the designated FTP server.

Please ensure that you have the required permissions and access to the network and FTP server.

Feel free to modify the script as per your specific requirements and network configuration.

## Resources

- Python documentation:
  - [subprocess](https://docs.python.org/3/library/subprocess.html)
  - [ipaddress](https://docs.python.org/3/library/ipaddress.html)
  - [openpyxl](https://openpyxl.readthedocs.io/)
  - [ftplib](https://docs.python.org/3/library/ftplib.html)
