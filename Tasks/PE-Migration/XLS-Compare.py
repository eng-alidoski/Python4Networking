import pandas as pd

# Read the first Excel sheet
df1 = pd.read_excel('ip_reachability.xlsx')

# Read the second Excel sheet
df2 = pd.read_excel('ip_reachability-2.xlsx')

# Compare the two sheets
if df1.equals(df2):
    print("The sheets have the same content.")
else:
    print("The sheets have different content.")
