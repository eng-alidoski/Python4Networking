# Pre and Post Migration Script for PE1 Router Migration

This script is used to ensure a smooth migration of our PE1 router from Cisco to Nokia. It validates the functionality of all given services to our customers. The script performs the following tasks:

- Checks the reachability of IP addresses within specified ranges.
- Saves reachable IP addresses to an Excel file.
- Uploads the file to an FTP server.

## Before running the script
- Provide the IP ranges and configure FTP server details.
- Install dependencies: `openpyxl` and `ftplib`.

## Running the script
To run the script, execute the Python script using a Python interpreter. It iterates over the IP ranges, pings each IP address, and records reachable IPs in the Excel file. The file is then uploaded to the FTP server.

By using this script, we can validate the migration process and ensure the functionality of all services during the transition from Cisco to Nokia for our PE1 router.
