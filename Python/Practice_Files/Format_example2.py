platform = input("what is your platform: ")
show_commands = input("what show commands do you want to send: ")
platform_to_test = platform.lower()

if platform_to_test == "cisco":
    commands_to_send = f"enable\n{show_commands}\n"
    print(commands_to_send)
