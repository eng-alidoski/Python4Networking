int_list = ["interface gig0/0","interface gig0/1","interface gig0/2","interface loopback 0"]

for interface in int_list:
    if interface == "interface gig0/0":
        continue
    print(f"{interface}\n ip ospf 1 area 0\n")   #\n new line