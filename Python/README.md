# Introduction

This is my testing python area 

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install necessary tools.

```bash
apt-get update
apt-get install python -y
apt-get install build-essential libssl-dev libffi-dev -y
apt-get install python-pip -y
pip3 install ipython
pip install cryptography
pip install netmiko
pip install napalm
pip install telnetlib3
python3 -m pip install pylint
python3 -m pip install black
```

## Extensions that i am using with VS Code  

```python
#cisco ios syntax highlighter 
#better jinja
#Monokai Pro theme
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.
